"configuracion en vim
set rnu
"set number
syntax on
set autoindent
set smartindent
set history=5000
set number
set cursorline
set mouse=a
set clipboard=unnamedplus
set tabstop=2 softtabstop=2 expandtab shiftwidth=2
set foldmethod=syntax
set foldlevel=99
set numberwidth=1
"set nowrap
set noswapfile
set nobackup
"set incsearch
"set ignorecase
set encoding=utf-8
colorscheme molokai
filetype plugin on
set omnifunc=syntaxcomplete#Complete
let mapleader = "\<Space>"

inoremap <c-o> </<c-x><c-o><esc>
"inoremap <F8> <cr><c-o>O<tab>

nnoremap <leader>q :qall!<CR>
nnoremap <leader>w :wqall<cr>
nnoremap ,html :-1read $HOME/.vim/templates/.skeleton.html<cr>

inoremap <F4> <esc>:wall<cr>
nnoremap <F4> :wall<cr>

inoremap <F8> <cr></<c-x><c-o><esc><up>f"lyi"O<esc>pA--><esc>I<!--<esc>yy<down><down>p02f-aFIN<tab><esc><up><up>o<tab>
inoremap {} {}<esc>i
inoremap }} <space>{<cr>}<esc>O
inoremap "" ""<esc>i
inoremap [] []<esc>i
inoremap () ()<esc>i
inoremap <> <><esc>i
